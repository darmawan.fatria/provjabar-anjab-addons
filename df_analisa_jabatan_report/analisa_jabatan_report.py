from openerp.osv import fields, osv


class analisa_jabatan_report(osv.osv_memory):
    
    _name = "analisa.jabatan.report"
    _columns = {
        'company_id': fields.many2one('res.company', 'OPD',),
       }
    
    def get_analisa_jabatan_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'analisa.jabatan.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'analisa.jabatan.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
analisa_jabatan_report()
