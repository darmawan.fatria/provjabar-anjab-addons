# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-07-11 : 1. Init
#  
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp

class template_uraian_tugas_jabatan(osv.Model):
    _name = "template.uraian.tugas.jabatan"
    _description = "Template Uraian Tugas"
    _columns = {
       # 'notes'		: fields.text("Uraian", required=True ),
        'name'		        : fields.char("Uraian Tugas", size=256,required=True),
        'template_id'       : fields.many2one('template.jabatan', 'Template',required=False),
        'standar_kompetensi_ids' : fields.one2many('template.standar.kompetensi.uraian','uraian_tugas_id',string='Standar Kompetensi'),
        'indikator_kinerja_jabatan_ids' : fields.one2many('template.indikator.kinerja.uraian','uraian_tugas_id',string='Indikator Kinerja',help='Sesuai Indikator yang dipilih di jabatan'),
        'tahapan_kerja_ids' : fields.one2many('template.tahapan.kerja.jabatan','uraian_tugas_id',string='Tahapan Kerja'),

    }

template_uraian_tugas_jabatan()
class template_indikator_kinerja_uraian(osv.Model):
    _name = "template.indikator.kinerja.uraian"
    _description = "Template Indikator Kinerja Uraian"
    _columns = {
        'name'      : fields.char("Indikator Kinerja",size=216, required=True ),
        'uraian_tugas_id': fields.many2one('template.uraian.tugas.jabatan', 'Template',required=False),
    }
template_indikator_kinerja_uraian()
class template_standar_kompetensi_uraian(osv.Model):
    _name = "template.standar.kompetensi.uraian"
    _description = "Template Standar Kompetensi Uraian"
    _columns = {
        'name'      : fields.char("Standar Kompetensi",size=216, required=True ),
        'jenis_standar_kompetensi'                  : fields.selection([('manajerial', 'Manajerial'),
                                                     ('teknis', 'Teknis'),
                                                      ('sosiokultural', 'Sosiokultural'),
                                                       ('pemerintahan', 'Pemerintahan')], 
                                            'Jenis Standar Kompetensi',required=True),
        'uraian_tugas_id': fields.many2one('template.uraian.tugas.jabatan', 'Uraian Tugas',required=False),
    }
template_standar_kompetensi_uraian()
