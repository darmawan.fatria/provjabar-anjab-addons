# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2015 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

class partner_employee_eselon(osv.Model):
    _inherit = "partner.employee.eselon"
    
    _columns = {
        'min_level_kompetensi': fields.integer("Batas Bawah Level Kompetensi",),
        'max_level_kompetensi': fields.integer("Batas Atas Level Kompetensi",),
     
    }
    
    
partner_employee_eselon()