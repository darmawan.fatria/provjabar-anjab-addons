# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-07-11 : 1. Init
#  
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class template_tahapan_kerja_jabatan(osv.Model):
    _name = "template.tahapan.kerja.jabatan"
    _description = "Template Tahapan Kerja Uraian Tugas"
    _columns = {
        'name'      : fields.char("Tahapan",size=216, required=True ),
        'uraian_tugas_id': fields.many2one('template.uraian.tugas.jabatan', 'Template',required=False),
        'hasil_kerja_ids'           : fields.one2many('template.hasil.kerja.jabatan','tahapan_id',string='Hasil Kerja'),
        'bahan_kerja_ids'           : fields.one2many('template.bahan.kerja.jabatan','tahapan_id',string='Bahan Kerja'),
        'perangkat_kerja_ids'       : fields.one2many('template.perangkat.kerja.jabatan','tahapan_id',string='Perangkat Kerja'),
        'tgjawab_kerja_ids': fields.one2many('template.tgjawab.jabatan','tahapan_id',string='Tanggung Jawab Kerja'),
        'wewenang_kerja_ids': fields.one2many('template.wewenang.jabatan','tahapan_id',string='Wewenang Kerja'),
        'kondisi_ids'          : fields.one2many('template.kondisi.jabatan','tahapan_id',string='Kondisi Ligkungan Kerja'),
        'resiko_ids'           : fields.one2many('template.resiko.jabatan','tahapan_id',string='Resiko Bahaya Ligkungan Kerja'),
        'waktu_kerja'           : fields.integer('Waktu Penyelesaian (Menit) ',required=False),
        'anggaran_kerja'           : fields.float('Anggaran Kerja ',required=False),
        #this!!!!anggaran'          : fields.integer('Waktu Kerja Efektif (Menit) ',required=True),
       
        'korelasi_internal_ids'          : fields.one2many('template.korelasi.internal','tahapan_id',string='Korelasi Internal'),
        'korelasi_eksternal_ids'           : fields.one2many('template.korelasi.eksternal','tahapan_id',string='Korelasi Eksternal'),
    }
template_tahapan_kerja_jabatan()


class template_hasil_kerja_jabatan(osv.Model):
    _name = "template.hasil.kerja.jabatan"
    _description = "Template Hasil Kerja"
    _columns = {
        'name'      : fields.char("Hasil Kerja",size=216, required=True ),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),
   
        
    }
template_hasil_kerja_jabatan()

class template_bahan_kerja_jabatan(osv.Model):
    _name = "template.bahan.kerja.jabatan"
    _description = "Template Bahan Kerja"
    _columns = {
        'name'      : fields.char("Bahan Kerja",size=216, required=True ),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),
        
        
    }
template_bahan_kerja_jabatan()

class template_perangkat_kerja_jabatan(osv.Model):
    _name = "template.perangkat.kerja.jabatan"
    _description = "Template Perangkat Kerja"
    _columns = {
        'name'      : fields.char("Perangkat Kerja",size=216, required=True ),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),  
    }
template_perangkat_kerja_jabatan()

class template_tgjawab_jabatan(osv.Model):
    _name = "template.tgjawab.jabatan"
    _description = "Template Tanggung Jawab"
    _columns = {
        'name'      : fields.char("Tanggung Jawab",size=216, required=True ),
      'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),  
    }
template_tgjawab_jabatan()
class template_wewenang_jabatan(osv.Model):
    _name = "template.wewenang.jabatan"
    _description = "Template Wewenang"
    _columns = {
        'name'      : fields.char("Wewenang Kerja",size=216, required=True ),
         'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),  
    }
template_wewenang_jabatan()

class template_kondisi_jabatan(osv.Model):
    _name = "template.kondisi.jabatan"
    _description = "Template Kondisi Lingkungan Kerja Jabatan"
    _columns = {
     'name'      : fields.char("Aspek",size=216, required=True ),
     'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),  
    }
template_kondisi_jabatan()
class template_resiko_jabatan(osv.Model):
    _name = "template.resiko.jabatan"
    _description = "Template Resiko Bahaya Kerja Jabatan"
    _columns = {
        'name'      : fields.char("Fisik/Mental",size=216, required=True ),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),  
    }
template_resiko_jabatan()

class template_korelasi_internal(osv.Model):
    _name = "template.korelasi.internal"
    _description = "Template Korelasi Internal"
    _columns = {
        'relation_template_id': fields.many2one('template.jabatan', 'Bawahan',required=True),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),
         'name'      : fields.char("Catatan",size=128 ),
         'jenis_korelasi'                  : fields.selection([('atasan', 'Atasan'),
                                                     ('bawahan', 'Bawahan'),
                                                      ('diagonal', 'Diagonal'),
                                                       ('setara', 'Setara')], 
                                            'Jenis Korelasi',required=True),
    }
template_korelasi_internal()
class template_korelasi_eksternal(osv.Model):
    _name = "template.korelasi.eksternal"
    _description = "Template Korelasi Eksternal"
    _columns = {
        'relation_template_id': fields.many2one('template.jabatan', 'Bawahan',required=True),
        'tahapan_id': fields.many2one('template.tahapan.kerja.jabatan', 'Tahapan',required=True),
         'name'      : fields.char("Catatan",size=128 ),
         'jenis_korelasi'                  : fields.selection([('atasan', 'Atasan'),
                                                     ('bawahan', 'Bawahan'),
                                                      ('diagonal', 'Diagonal'),
                                                       ('setara', 'Setara')], 
                                            'Jenis Korelasi',required=True),
    }
template_korelasi_eksternal()