# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-07-11 : 1. Init
#  
##############################################################################

from openerp.osv import fields
from openerp.osv import osv

class uraian_tugas_jabatan(osv.Model):
    _name = "uraian.tugas.jabatan"
    _description = "Uraian Tugas Jabatan"
    _columns = {
       # 'notes'		: fields.text("Uraian", required=True ),
        'name'		        : fields.char("Uraian Tugas", size=256,required=False),
        'template_id'       : fields.many2one('informasi.jabatan', 'Template',required=False),
        'template_uraian_id'       : fields.many2one('template.uraian.tugas.jabatan', 'Template Uraian Tugas Jabatan',required=False),
        'is_use'        : fields.boolean('Pilih'),
        'is_input_indikator_kinerja'        : fields.boolean('Input Indikator Kinerja'), #digunakan saat stage isi uraian tugas
        'indikator_ids' : fields.many2many('indikator.kinerja.jabatan','indikator_uraian_rel','uraian_id', 'indikator_id',string='Indikator Kinerja Jabatan',help='Sesuai Renstra'),
        

        #is use?
        'standar_minimal_manajerial'    : fields.one2many('standar.kompetensi.jabatan','uraian_id',string='Std.Minimal Manajerial'),
        'standar_minimal_teknis'        : fields.one2many('standar.kompetensi.jabatan','uraian_id',string='Std.Minimal Teknis'),
        'standar_minimal_sosiokultural' : fields.one2many('standar.kompetensi.jabatan','uraian_id',string='Std.Minimal Sosiokultural'),
        'standar_minimal_pemerintahan'  : fields.one2many('standar.kompetensi.jabatan','uraian_id',string='Std.Minimal Pemerinahan'),
    }
    _defaults = {
        'is_use': False,
        'is_input_indikator_kinerja' :False
    }

uraian_tugas_jabatan()
class standar_kompetensi_jabatan(osv.Model):
    _name = "standar.kompetensi.jabatan"
    _description = "Standar Kompetensi Jabatan"
    _columns = {
        'template_id'       : fields.many2one('informasi.jabatan', 'Template',required=False),
        'uraian_id'         : fields.many2one('uraian.tugas.jabatan', 'Uraian',required=False),
        'unit_manajerial_ids'    : fields.one2many('unit.standar.kompetensi.uraian','standar_kompetensi_id',string='Manajerial',domain=[('jenis_standar_kompetensi','=','manajerial')]),
        'unit_teknis_ids'    : fields.one2many('unit.standar.kompetensi.uraian','standar_kompetensi_id',string='Teknis',domain=[('jenis_standar_kompetensi','=','teknis')]),
        'unit_sosiokultural_ids'    : fields.one2many('unit.standar.kompetensi.uraian','standar_kompetensi_id',string='Sosiokultural',domain=[('jenis_standar_kompetensi','=','sosiokultural')]),
        'unit_pemerintahan_ids'    : fields.one2many('unit.standar.kompetensi.uraian','standar_kompetensi_id',string='Pemerintahan',domain=[('jenis_standar_kompetensi','=','pemerintahan')]),
        
        'is_manajerial'             : fields.boolean('Manajerial'),
        'is_teknis'                 : fields.boolean('Teknis'),
        'is_sosiokultural'          : fields.boolean('Sosiokultural'),
        'is_pemerintahan'           : fields.boolean('Pemerintahan'),


        'min_level_kompetensi': fields.integer("Batas Bawah Level Kompetensi",),
        'max_level_kompetensi': fields.integer("Batas Atas Level Kompetensi",),

        'active'        : fields.boolean('Active'),
    }
_defaults = {
      'active' :True,
  }

def set_cancel(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'active':False}, context=context) 

standar_kompetensi_jabatan()
class unit_standar_kompetensi_uraian(osv.Model):
    _name = "unit.standar.kompetensi.uraian"
    _description = "Unit Standar Kompetensi Tiap Uraian"

    def onchange_unit_standar_kompetensi(self, cr, uid, ids, unit_id, context=None):
        res = {'value': {'level_min_id': None,
                                             'level_id': None,
                         }}
        return res

    _columns = {
        'standar_kompetensi_id'         : fields.many2one('standar.kompetensi.jabatan', 'Std. Kompetensi Uraian',required=False),
        'unit_id'         : fields.many2one('unit.standar.kompetensi.jabatan', 'Unit',required=False,),
        'level_min_id'         : fields.many2one('level.standar.kompetensi.jabatan', 'Std. Minimal',required=False),
        'level_id'         : fields.many2one('level.standar.kompetensi.jabatan', 'Anda Saat Ini',required=False),
        'jenis_standar_kompetensi'                  : fields.selection([('manajerial', 'Manajerial'),
                                                     ('teknis', 'Teknis'),
                                                      ('sosiokultural', 'Sosiokultural'),
                                                       ('pemerintahan', 'Pemerintahan')], 
                                            'Jenis Standar Kompetensi',required=True),
    }
   
unit_standar_kompetensi_uraian()
class unit_standar_kompetensi_jabatan(osv.Model):
    _name = "unit.standar.kompetensi.jabatan"
    _description = "Unit Standar Kompetensi Jabatan"
    _columns = {
        'name'      : fields.char("Unit Standar Kompetensi",size=50, required=True ),
          'description'      : fields.text("Deskripsi", required=False ),
        'jenis_standar_kompetensi'                  : fields.selection([('manajerial', 'Manajerial'),
                                                     ('teknis', 'Teknis'),
                                                      ('sosiokultural', 'Sosiokultural'),
                                                       ('pemerintahan', 'Pemerintahan')], 
                                            'Jenis Standar Kompetensi',required=True),
        'jenis_manajerial'                  : fields.selection([('kb', 'Kemampuan Berpikir (KB)'),
                                                     ('md', 'Mengelola Diri (MD)'),
                                                      ('mo', 'Mengelola Orang Lain (MO)'),
                                                      ('sb', 'Mengelola Sosial dan Budaya (SB)'),
                                                       ('mt', 'Mengelola Tugas (MT)')], 
                                            'Kategori Manajerial',required=False),
    }

    # def create(self, cr, uid, vals, context=None):
            
    #         new_unit_id = super(unit_standar_kompetensi_jabatan, self).create(cr, uid, vals, context)
    #         print "Vals : ",vals
    #         print "new_unit_id : ",new_unit_id
    #         if new_unit_id:
                
    #             level_std_pool = self.pool.get('level.standar.kompetensi.jabatan');
    #             jenis_standar_kompetensi_val = vals['jenis_standar_kompetensi']
    #             level_data =  {}

    #             level_data.update({ 'unit_kompetensi_id':new_unit_id,
    #                                          'jenis_standar_kompetensi':jenis_standar_kompetensi_val,
    #                                 })
    #             for i in range(1,6):
    #                 level_data.update({ 'name':'Level '+str(i),
    #                                          'level':'Level '+str(i),
    #                                           'level_value': float(i),
    #                                 })
    #                 level_std_pool.create(cr, SUPERUSER_ID, level_data,context)
                
    #         return True
   
unit_standar_kompetensi_jabatan()
class level_standar_kompetensi_jabatan(osv.Model):
    _name = "level.standar.kompetensi.jabatan"
    _description = "Standar Kompetensi Jabatan"
    _columns = {
        'name'              : fields.char("Uraian",size=216, required=True ),
        'level'              : fields.char("Level",size=15, required=True ),
        'level_value'      : fields.float("Nilai", required=True  ),
        'unit_kompetensi_id'         : fields.many2one('unit.standar.kompetensi.jabatan', 'Unit',required=True),
         'jenis_standar_kompetensi'                  : fields.selection([('manajerial', 'Manajerial'),
                                                                    ('teknis', 'Teknis'),
                                                                    ('sosiokultural', 'Sosiokultural'),
                                                                     ('pemerintahan', 'Pemerintahan')], 
                                                         'Jenis Standar Kompetensi',required=True),
        
    }
    _order = "name"
level_standar_kompetensi_jabatan()


class uraian_hasil_kerja_jabatan(osv.Model):
    _name = "uraian.hasil.kerja.jabatan"
    _description = "Hasil Kerja Uraian Jabatan"
    
    def get_total_anggaran_kerja(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.hasil_kerja_uraian_ids:
                if child.anggaran_kerja_id :
                    total_value+= child.anggaran_kerja_id.value;
           res[data.id] = total_value
        return res

    def get_total_waktu_anggaran_kerja(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.hasil_kerja_uraian_ids:
                  total_value+= child.waktu_kerja;
           res[data.id] = total_value
        return res

    _columns = {
        'template_id'       : fields.many2one('informasi.jabatan', 'Template',required=False),
        'uraian_id'         : fields.many2one('uraian.tugas.jabatan', 'Uraian',required=False),
        'job_type'                  : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=False),
        
        #'total_waktu_kerja'       : fields.integer('Total Waktu Penyelesaian (Menit) ',required=False),
        #'total_anggaran_kerja'    : fields.float('Total Anggaran Kerja (Rp)',required=False),
        'total_anggaran_kerja'    : fields.function(get_total_anggaran_kerja, method=True, string='Total Anggaran Kerja (Rp)', type='float', readonly=True,store=False),
        'total_waktu_kerja'    : fields.function(get_total_waktu_anggaran_kerja, method=True, string='Total Waktu Penyelesaian Dlm 1 Tahun (Menit)', type='integer', readonly=True,store=False),
        'hasil_kerja_uraian_ids'         : fields.one2many('hasil.kerja.jabatan','uraian_id', string='Hasil Kerja',required=False),
        'kondisi_ids'          : fields.one2many('uraian.kondisi.jabatan','uraian_id',string='Kondisi Ligkungan Kerja'),
        'resiko_ids'           : fields.one2many('uraian.resiko.jabatan','uraian_id',string='Resiko Bahaya Ligkungan Kerja'),
        'korelasi_ids'          : fields.one2many('uraian.korelasi.jabatan','uraian_id',string='Korelasi Pekerjaan'),
        'active'  : fields.boolean('Active'),
    }
   
uraian_hasil_kerja_jabatan()
class hasil_kerja_jabatan(osv.Model):
    _name = "hasil.kerja.jabatan"
    _description = "Hasil Kerja Jabatan"
    _columns = {
        'uraian_id'         : fields.many2one('uraian.hasil.kerja.jabatan', 'Uraian',required=False),
        'waktu_kerja'       : fields.integer('Waktu Penyelesaian Dlm 1 Tahun (Menit) ',required=False),
        #'anggaran_kerja'    : fields.float('Anggaran Kerja (Rp)',required=False),
        'anggaran_kerja_id'          : fields.many2one('anggaran.hasil.kerja', 'Anggaran',required=False),
        'hasil_id'          : fields.many2one('item.hasil.kerja', 'Hasil',required=False),
        'jumlah_satuan'     : fields.integer('Jumlah'),
        'satuan_id'         : fields.many2one('satuan.hasil.kerja', 'Satuan',required=False),
        'bahan_id'          : fields.many2many('bahan.hasil.kerja', string='Bahan',required=False),
        'penggunaan_bahan_id'   : fields.many2many('penggunaan.bahan.hasil.kerja', string='Penggunaan Bahan',required=False),
        'perangkat_id'      : fields.many2many('perangkat.hasil.kerja',  string='Perangkat',required=False),
        'penggunaan_perangkat_id'     : fields.many2many('penggunaan.perangkat.hasil.kerja', string='Penggunaan Perangkat',required=False),
        'tanggung_jawab'    : fields.many2many('tanggung.jawab.hasil.kerja',string="Tanggung Jawab"),
        'wewenang'          : fields.many2many('wewenang.hasil.kerja',string="Wewenang"),
    }
   
hasil_kerja_jabatan()

class item_hasil_kerja(osv.Model):
    _name = "item.hasil.kerja"
    _description = "Item Hasil Kerja"
    _columns = {
        'name'      : fields.char("Hasil Kerja",size=216, required=True ),
        'satuan_key'      : fields.char("Satuan",size=216, required=False ,invisible=True),
    }
item_hasil_kerja()
class satuan_hasil_kerja(osv.Model):
    _name = "satuan.hasil.kerja"
    _description = "Satuan Hasil Kerja"
    _columns = {
        'name'      : fields.char("Satuan",size=75, required=True ),
    }
satuan_hasil_kerja()
class bahan_hasil_kerja(osv.Model):
    _name = "bahan.hasil.kerja"
    _description = "Bahan Hasil Kerja"
    _columns = {
        'name'      : fields.char("Bahan",size=75, required=True ),
        'satuan_key'      : fields.char("Satuan",size=216, required=False ,invisible=True),
    }
bahan_hasil_kerja()
class penggunaan_bahan_hasil_kerja(osv.Model):
    _name = "penggunaan.bahan.hasil.kerja"
    _description = "Penggunaan Bahan Hasil Kerja"
    _columns = {
        'name'      : fields.char("Penggunaan Bahan",size=216, required=True ),
    }
penggunaan_bahan_hasil_kerja()
class perangkat_hasil_kerja(osv.Model):
    _name = "perangkat.hasil.kerja"
    _description = "Perangkat Hasil Kerja"
    _columns = {
        'name'      : fields.char("Perangkat",size=75, required=True ),
    }
perangkat_hasil_kerja()
class penggunaan_perangkat_hasil_kerja(osv.Model):
    _name = "penggunaan.perangkat.hasil.kerja"
    _description = "Penggunaan Perangkat Hasil Kerja"
    _columns = {
        'name'      : fields.char("Penggunaan Perangkat",size=216, required=True ),
    }
penggunaan_perangkat_hasil_kerja()
class tanggung_jawab_hasil_kerja(osv.Model):
    _name = "tanggung.jawab.hasil.kerja"
    _description = "Tanggung Jawab Hasil Kerja"
    _columns = {
        'name'      : fields.char("Tanggung  Jawab",size=216, required=True ),
    }
tanggung_jawab_hasil_kerja()
class wewenang_hasil_kerja(osv.Model):
    _name = "wewenang.hasil.kerja"
    _description = "Wewenang Hasil Kerja"
    _columns = {
        'name'      : fields.char("Wewenang",size=216, required=True ),
    }
wewenang_hasil_kerja()
class anggaran_hasil_kerja(osv.Model):
    _name = "anggaran.hasil.kerja"
    _description = "Wewenang Hasil Kerja"
    _columns = {
        'name'      : fields.char("Anggaran",size=50, required=True ),
        'value'     : fields.float("Nilai Anggaran",required=True)
    }
anggaran_hasil_kerja()
class uraian_kondisi_jabatan(osv.Model):
    _name = "uraian.kondisi.jabatan"
    _description = "Uraian Kondisi Lingkungan Kerja Jabatan"

    def onchange_kondisi(self, cr, uid, ids, kondisi_id, context=None):
        res = {'value': {'faktor_id': None,'exist_faktor_id': None,}}
        return res

    _columns = {
     'kondisi_id'      : fields.many2one('kondisi.lingkungan.kerja', 'Aspek',required=False),
     'faktor_id'      : fields.many2one('faktor.kondisi.lingkungan.kerja', 'Faktor',required=False ,domain="[('kondisi_id','=',kondisi_id)]"),
     'exist_faktor_id'      : fields.many2one('faktor.kondisi.lingkungan.kerja', 'Fakta',required=False ,domain="[('kondisi_id','=',kondisi_id)]"),
     'uraian_id'         : fields.many2one('uraian.hasil.kerja.jabatan', 'Uraian',required=False),
    }
uraian_kondisi_jabatan()
class kondisi_lingkungan_kerja(osv.Model):
    _name = "kondisi.lingkungan.kerja"
    _description = "Tahapan Kondisi Lingkungan Kerja Jabatan"
    _columns = {
     'name'      : fields.char("Aspek",size=50, required=True ),
    }
class faktor_kondisi_lingkungan_kerja(osv.Model):
    _name = "faktor.kondisi.lingkungan.kerja"
    _description = "Faktor Kondisi Lingkungan Kerja Jabatan"
    _columns = {
     'name'      : fields.char("Faktor",size=50, required=True ),
     'kondisi_id'         : fields.many2one('kondisi.lingkungan.kerja', 'Aspek',required=False),
    }
class uraian_resiko_jabatan(osv.Model):
    _name = "uraian.resiko.jabatan"
    _description = "Tahapan Resiko Bahaya Kerja Jabatan"

    def onchange_bentuk(self, cr, uid, ids, bentuk_id, context=None):
        res = {'value': {'resiko_id': None,}}
        return res
    _columns = {
        'bentuk_id'      : fields.many2one('bentuk.resiko.kerja', 'Bentuk',required=False),
        'resiko_id'      : fields.many2one('penyebab.bentuk.resiko.kerja', 'Penyebab',required=False ,domain="[('bentuk_id','=',bentuk_id)]"),
        'uraian_id'         : fields.many2one('uraian.hasil.kerja.jabatan', 'Uraian',required=False), 
    }
uraian_resiko_jabatan()
class bentuk_resiko_kerja(osv.Model):
    _name = "bentuk.resiko.kerja"
    _description = "Tahapan Kondisi Lingkungan Kerja Jabatan"
    _columns = {
     'name'      : fields.char("Bentuk",size=150, required=True ),
    }
bentuk_resiko_kerja()
class penyebab_bentuk_resiko_kerja(osv.Model):
    _name = "penyebab.bentuk.resiko.kerja"
    _description = "Penyebab Resiko Kerja Jabatan"
    _columns = {
     'name'      : fields.char("Penyebab",size=150, required=True ),
     'bentuk_id'         : fields.many2one('bentuk.resiko.kerja', 'Bentuk',required=False),
    }
penyebab_bentuk_resiko_kerja()
class uraian_korelasi_jabatan(osv.Model):
    _name = "uraian.korelasi.jabatan"
    _description = "Korelasi ajabatan uraian"
    _columns = {
        'relation_jabatan_id': fields.many2one('template.jabatan', 'Jabatan',required=False),
         'name'      : fields.char("Nama Jabatan",size=150,  ),
         'uraian_id'         : fields.many2one('uraian.hasil.kerja.jabatan', 'Uraian',required=False),
         'jenis_korelasi'                  : fields.selection([('internal', 'Internal'),
                                                     ('eksternal', 'Eksternal'),
                                                     ], 
                                            'Jenis Korelasi',required=True),
        'company_id': fields.many2one('res.company', 'OPD',),
        'department_id': fields.many2one('partner.employee.department', 'Unit Kerja',),
        'eksternal_department'      : fields.char("Unit Kerja",size=150,  ),
        'jenis_instansi_id': fields.many2one('jenis.instansi', 'Instansi',required=False),
        'perihal_id': fields.many2one('perihal.korelasi.jabatan', 'Dalam Hal',required=True),
    }
uraian_korelasi_jabatan()
class jenis_instansi(osv.Model):
    _name = "jenis.instansi"
    _description = "Jenis Instansi"
    _columns = {
     'name'      : fields.char("Jenis Instansi",size=50, required=True ),
    }
jenis_instansi()
class perihal_korelasi_jabatan(osv.Model):
    _name = "perihal.korelasi.jabatan"
    _description = "Perihal Korelasi Jabatan"
    _columns = {
     'name'      : fields.char("Perihal Korelasi Jabatan",size=50, required=True ),
    }
perihal_korelasi_jabatan()