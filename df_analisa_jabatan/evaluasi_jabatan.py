# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-07-04 : 1. Init
#  
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID

#======================== Evaluasi Jabatan ====================#
class uraian_evaluasi_jabatan(osv.Model):
    _name = "uraian.evaluasi.jabatan"
    _description = "Uraian Evaluasi Jabatan"

    def _get_faktor_rl_dampak_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_rl_dampak:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_pengaturan_org_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_pengaturan_org:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_wewenang_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_wewenang:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_hub_personal_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_hub_personal:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_kesulitan_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_kesulitan:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_kond_lain_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_kond_lain:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_pengetahuan_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_pengetahuan:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_pengawasan_penyelia_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_pengawasan_penyelia:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_pedoman_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_pedoman:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_kompleksitas_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_kompleksitas:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_tujuan_hubungan_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_tujuan_hubungan:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    # def _get_faktor_sifat_hubungan_value(self, cr, uid, ids, field_names, args, context=None):
    #     res = {}
    #     for data in self.browse(cr, uid, ids, context=context):
    #        total_value = 0;
    #        for child in data.faktor_sifat_hubungan:
    #             if child.level_min_id and child.level_min_value:
    #                 total_value+= child.level_min_value;
    #        res[data.id] = total_value
    #     return res
    def _get_faktor_persyaratan_fisik_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_persyaratan_fisik:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_faktor_lingkungan_pekerjaan_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           total_value = 0;
           for child in data.faktor_lingkungan_pekerjaan:
                if child.level_min_id and child.level_min_value:
                    total_value+= child.level_min_value;
           res[data.id] = total_value
        return res
    def _get_total_nilai_uraian_value(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        
        for data in self.browse(cr, uid, ids, context=context):
            nilai_uraian = 0;
            pembagi =0;
            
            if data.faktor_rl_dampak :
                pembagi+=1;
            if data.faktor_pengaturan_org:
                pembagi+=1;
            if data.faktor_wewenang:
                pembagi+=1;
            if data.faktor_hub_personal:
                pembagi+=1;
            if data.faktor_kesulitan:
                pembagi+=1;
            if data.faktor_kond_lain:
                pembagi+=1;
            if data.faktor_pengetahuan:
                pembagi+=1;
            if data.faktor_pengawasan_penyelia:
                pembagi+=1;
            if data.faktor_pedoman:
                pembagi+=1;
            if data.faktor_tujuan_hubungan:
                pembagi+=1;
            if data.faktor_persyaratan_fisik:
                pembagi+=1;
            if data.faktor_lingkungan_pekerjaan:
                pembagi+=1;
            nilai_uraian = data.faktor_rl_dampak_value + data.faktor_pengaturan_org_value  + data.faktor_wewenang_value  + data.faktor_hub_personal_value   + data.faktor_kesulitan_value + data.faktor_kond_lain_value + data.faktor_pengetahuan_value + data.faktor_pengawasan_penyelia_value + data.faktor_pedoman_value + data.faktor_kompleksitas_value + data.faktor_tujuan_hubungan_value + data.faktor_persyaratan_fisik_value + data.faktor_lingkungan_pekerjaan_value 
            if pembagi > 0 :
                nilai_uraian = nilai_uraian/pembagi;

            res[data.id] = nilai_uraian
        return res

    _columns = {
        'template_id'       : fields.many2one('informasi.jabatan', 'Template',required=False),
        'uraian_id'       : fields.many2one('uraian.tugas.jabatan', 'Uraian',required=False),
        'job_type'                  : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=False),
        'total_nilai_uraian' :fields.function(_get_total_nilai_uraian_value, method=True, string='Nilai Uraian', type='float', readonly=True,store=False),

        'faktor_rl_dampak' : fields.one2many('evaluasi.ruang.lingkup.uraian','evaluasi_id',string='Ruang Lingkup'),
        'faktor_rl_dampak_value' :fields.function(_get_faktor_rl_dampak_value, method=True, string='Nilai Ruang Lingkup', type='float', readonly=True,store=False),
        
        'faktor_pengaturan_org' : fields.one2many('evaluasi.pengaturan.organisasi.uraian','evaluasi_id',string='Pengaturan Organisasi'),
        'faktor_pengaturan_org_value' :fields.function(_get_faktor_pengaturan_org_value, method=True, string='Nilai Pengaturan', type='float', readonly=True,store=False),
        
        'faktor_wewenang' : fields.one2many('evaluasi.wewenang.penyeliaan.uraian','evaluasi_id',string='Wewenang Kepenyeliaan'),
        'faktor_wewenang_value' :fields.function(_get_faktor_wewenang_value, method=True, string='Nilai Wewenang', type='float', readonly=True,store=False),

        'faktor_hub_personal' : fields.one2many('evaluasi.hubungan.personal.uraian','evaluasi_id',string='Hubungan Personal'),
        'faktor_hub_personal_value' :fields.function(_get_faktor_hub_personal_value, method=True, string='Nilai Hubungan Personal', type='float', readonly=True,store=False),

        'faktor_kesulitan' : fields.one2many('evaluasi.kesulitan.pengarahan.uraian','evaluasi_id',string='Kesulitan Pengarahan'),
        'faktor_kesulitan_value' :fields.function(_get_faktor_kesulitan_value, method=True, string='Nilai Kesulitan', type='float', readonly=True,store=False),

        'faktor_kond_lain' : fields.one2many('evaluasi.kondisi.lain.uraian','evaluasi_id',string='Kondisi Lain'),
        'faktor_kond_lain_value' :fields.function(_get_faktor_kond_lain_value, method=True, string='Nilai Kondisi Lain', type='float', readonly=True,store=False),

        'faktor_pengetahuan' : fields.one2many('evaluasi.pengetahuan.uraian','evaluasi_id',string='Pengetahuan'),
        'faktor_pengetahuan_value' :fields.function(_get_faktor_pengetahuan_value, method=True, string='Nilai Wewenang', type='float', readonly=True,store=False),

        'faktor_pengawasan_penyelia' : fields.one2many('evaluasi.pengawasan.penyelia.uraian','evaluasi_id',string='Pengawasan Penyelia'),
        'faktor_pengawasan_penyelia_value' :fields.function(_get_faktor_pengawasan_penyelia_value, method=True, string='Nilai Pengawasn Penyelia', type='float', readonly=True,store=False),

        'faktor_pedoman' : fields.one2many('evaluasi.pedoman.uraian','evaluasi_id',string='Pedoman'),
        'faktor_pedoman_value' :fields.function(_get_faktor_pedoman_value, method=True, string='Nilai Pedoman', type='float', readonly=True,store=False),

        'faktor_kompleksitas' : fields.one2many('evaluasi.kompleksitas.uraian','evaluasi_id',string='Kompleksitas'),
        'faktor_kompleksitas_value' :fields.function(_get_faktor_kompleksitas_value, method=True, string='Nilai Kompleksitas', type='float', readonly=True,store=False),

        'faktor_tujuan_hubungan' : fields.one2many('evaluasi.tujuan.hubungan.uraian','evaluasi_id',string='Tujuan Hubungan'),
        'faktor_tujuan_hubungan_value' :fields.function(_get_faktor_tujuan_hubungan_value, method=True, string='Nilai Tujuan Hubungan', type='float', readonly=True,store=False),

        # 'faktor_sifat_hubungan' : fields.one2many('evaluasi.sifat.hubungan.uraian','evaluasi_id',string='Sifat Hubungan'),
        # 'faktor_sifat_hubungan_value' :fields.function(_get_faktor_tujuan_hubungan_value, method=True, string='Nilai Sifat Hubungan', type='float', readonly=True,store=False),


        'faktor_persyaratan_fisik' : fields.one2many('evaluasi.persyaratan.fisik.uraian','evaluasi_id',string='Persyaratan Fisik'),
        'faktor_persyaratan_fisik_value' :fields.function(_get_faktor_persyaratan_fisik_value, method=True, string='Nilai Persyaratan Fisik', type='float', readonly=True,store=False),

        'faktor_lingkungan_pekerjaan' : fields.one2many('evaluasi.lingkungan.pekerjaan.uraian','evaluasi_id',string='Lingkungan Pekerjaan'),   
        'faktor_lingkungan_pekerjaan_value' :fields.function(_get_faktor_lingkungan_pekerjaan_value, method=True, string='Nilai Lingkungan Pekerjaan', type='float', readonly=True,store=False),
        
        'active'        : fields.boolean('Active'),
    }
_defaults = {
      'active' :True,
  }
   
uraian_evaluasi_jabatan()
class sub_faktor_evaluasi_jabatan(osv.Model):
    _name = "sub.faktor.evaluasi.jabatan"
    _description = "level_sub_faktor_evaluasi_jabatan"
    _columns = {
        'name'              : fields.char("Sub Faktor",size=52, required=True ),
        'jenis_faktor_jabatan'                  : fields.selection([('rl_dampak', 'Ruang Lingkup dan Dampak'),
                                          ('pengaturan_org', 'Pengaturan Organisasi'),
                                          ('wewenang', 'Wewenang Penyeliaan dan Manajerial'),
                                          ('hub_personal', 'Hubungan Personal'),
                                          ('kesulitan', 'Kesesuaian dalam Pengarahan Pekerjaan'),
                                          ('kond_lain', 'Kondisi Lain'),
                                          ('pengetahuan', 'Pengetahuan'),
                                          ('pengawasan_penyelia', 'Pengawasan Penyelia'),
                                          ('pedoman', 'Pedoman'),
                                          ('kompleksitas', 'Kompleksitas'),
                                          ('tujuan_hubungan', 'Tujuan Hubungan'),
                                          # ('sifat_hubungan', 'Sifat Hubungan'),
                                          ('persyaratan_fisik', 'Persyaratan Fisik'),
                                          ('lingkungan_pekerjaan', 'Lingkungan Pekerjaan'),
                                                    ],'Jenis Faktor Jabatan',required=True),
  'job_type'                  : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=True),
    }   
sub_faktor_evaluasi_jabatan()
class level_sub_faktor_evaluasi_jabatan(osv.Model):
    _name = "level.sub.faktor.evaluasi.jabatan"
    _description = "level_sub_faktor_evaluasi_jabatan"
    _columns = {
        'name'                : fields.char("Uraian",size=216, required=True ),
        'level'                 : fields.char("Level",size=15, required=True ),
        'level_value'        : fields.float("Nilai", required=True  ),
        'eselon_id'           : fields.many2one('partner.employee.eselon', 'Eselon'),
        'job_type'            : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan ',),
        'sub_faktor_id'      : fields.many2one('sub.faktor.evaluasi.jabatan', 'Sub Faktor'),
        'jenis_faktor_jabatan'                  : fields.selection([('rl_dampak', 'Ruang Lingkup dan Dampak'),
                                          ('pengaturan_org', 'Pengaturan Organisasi'),
                                          ('wewenang', 'Wewenang Penyeliaan dan Manajerial'),
                                          ('hub_personal', 'Hubungan Personal'),
                                          ('kesulitan', 'Kesesuaian dalam Pengarahan Pekerjaan'),
                                          ('kond_lain', 'Kondisi Lain'),
                                          ('pengetahuan', 'Pengetahuan'),
                                          ('pengawasan_penyelia', 'Pengawasan Penyelia'),
                                          ('pedoman', 'Pedoman'),
                                          ('kompleksitas', 'Kompleksitas'),
                                          ('tujuan_hubungan', 'Tujuan Hubungan'),
                                          # ('sifat_hubungan', 'Sifat Hubungan'),
                                          ('persyaratan_fisik', 'Persyaratan Fisik'),
                                          ('lingkungan_pekerjaan', 'Lingkungan Pekerjaan'),
                                                    ],'Jenis Faktor Jabatan',required=True),
    }
    _order = "name"   
level_sub_faktor_evaluasi_jabatan()

class evaluasi_ruang_lingkup_uraian(osv.Model):
    _name = "evaluasi.ruang.lingkup.uraian"
    _description = "Evaluasi Ruang Lingkup"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,
                               
                         }}
        if level_id : 
          	for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
          		res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,
                               
                         }}
        if level_id : 
          	for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
          		res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_ruang_lingkup_uraian, self).create(cr, uid, vals, context)


    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','rl_dampak')],required=True, ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','rl_dampak')],required=True,),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','rl_dampak')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }



evaluasi_ruang_lingkup_uraian()
class evaluasi_pengaturan_organisasi_uraian(osv.Model):
    _name = "evaluasi.pengaturan.organisasi.uraian"
    _description = "Evaluasi Pengaturan Organisasi"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_pengaturan_organisasi_uraian, self).create(cr, uid, vals, context)
    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','pengaturan_org')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','pengaturan_org')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','pengaturan_org')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_pengaturan_organisasi_uraian()
class evaluasi_wewenang_penyeliaan_uraian(osv.Model):
    _name = "evaluasi.wewenang.penyeliaan.uraian"
    _description = "Evaluasi Wewenang Penyeliaan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_wewenang_penyeliaan_uraian, self).create(cr, uid, vals, context)

    _columns = {
         'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','wewenang')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','wewenang')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','wewenang')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_wewenang_penyeliaan_uraian()
class evaluasi_hubungan_personal_uraian(osv.Model):
    _name = "evaluasi.hubungan.personal.uraian"
    _description = "Evaluasi Hubungan Personal"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_hubungan_personal_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','hub_personal')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jab1atan','=','hub_personal')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','hub_personal')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_hubungan_personal_uraian()
class evaluasi_kesulitan_pengarahan_uraian(osv.Model):
    _name = "evaluasi.kesulitan.pengarahan.uraian"
    _description = "Evaluasi Kesulitan Pengarahan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_kesulitan_pengarahan_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','kesulitan')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','kesulitan')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','kesulitan')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_kesulitan_pengarahan_uraian()
class evaluasi_kondisi_lain_uraian(osv.Model):
    _name = "evaluasi.kondisi.lain.uraian"
    _description = "Evaluasi Kondisi Lain"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_kondisi_lain_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','kond_lain')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','kond_lain')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','kond_lain')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_kondisi_lain_uraian()
class evaluasi_pengetahuan_uraian(osv.Model):
    _name = "evaluasi.pengetahuan.uraian"
    _description = "Evaluasi Pengetahuan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_pengetahuan_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','pengetahuan')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','pengetahuan')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','pengetahuan')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_pengetahuan_uraian()
class evaluasi_pengawasan_penyelia_uraian(osv.Model):
    _name = "evaluasi.pengawasan.penyelia.uraian"
    _description = "Evaluasi Pengawasan Penyelia"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_pengawasan_penyelia_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','pengawasan_penyelia')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','pengawasan_penyelia')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','pengawasan_penyelia')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_pengawasan_penyelia_uraian()

class evaluasi_pedoman_uraian(osv.Model):
    _name = "evaluasi.pedoman.uraian"
    _description = "Evaluasi Pedoman"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_pedoman_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','pedoman')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','pedoman')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','pedoman')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_pedoman_uraian()
class evaluasi_kompleksitas_uraian(osv.Model):
    _name = "evaluasi.kompleksitas.uraian"
    _description = "Evaluasi Kompleksitas"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_kompleksitas_uraian, self).create(cr, uid, vals, context)

    _columns = {
       'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','kompleksitas')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','kompleksitas')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level',domain=[('jenis_faktor_jabatan','=','kompleksitas')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_kompleksitas_uraian()

class evaluasi_tujuan_hubungan_uraian(osv.Model):
    _name = "evaluasi.tujuan.hubungan.uraian"
    _description = "Evaluasi Tujuan Hubungan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_tujuan_hubungan_uraian, self).create(cr, uid, vals, context)

    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','tujuan_hubungan')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','tujuan_hubungan')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','tujuan_hubungan')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_tujuan_hubungan_uraian()
class evaluasi_sifat_hubungan_uraian(osv.Model):
    _name = "evaluasi.sifat.hubungan.uraian"
    _description = "Evaluasi Sifat Hubungan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_sifat_hubungan_uraian, self).create(cr, uid, vals, context)
           
    _columns = {
       'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','sifat_hubungan')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','sifat_hubungan')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','sifat_hubungan')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value'     : fields.float('Nilai Std.Minimal',readonly=True),
        'level_value'            : fields.float('Nilai Anda',readonly=True),
    }
evaluasi_sifat_hubungan_uraian()

class evaluasi_persyaratan_fisik_uraian(osv.Model):
    _name = "evaluasi.persyaratan.fisik.uraian"
    _description = "Evaluasi Persyaratan Fisik"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_persyaratan_fisik_uraian, self).create(cr, uid, vals, context)


    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','persyaratan_fisik')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','persyaratan_fisik')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','persyaratan_fisik')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value': fields.related('level_min_id','level_value', type='float', string='Nilai Std.Minimal', store=False,readonly=True),
        'level_value': fields.related('level_id','level_value', type='float', string='Nilai Anda', store=False,readonly=True),
    }
evaluasi_persyaratan_fisik_uraian()

class evaluasi_lingkungan_pekerjaan_uraian(osv.Model):
    _name = "evaluasi.lingkungan.pekerjaan.uraian"
    _description = "Evaluasi Lingkungan Pekerjaan"

    def onchange_sub_faktor(self, cr, uid, ids, sub_faktor_id, context=None):
        res = {'value': {'level_min_id': None,
                                  'level_id': None,
                         }}
        return res
    def on_change_level_min(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_min_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_min_value'] =level_obj.level_value   
        return res
    def on_change_level(self, cr, uid, ids, level_id, context=None):
        res = {'value': {'level_value': 0,}}
        if level_id : 
            for level_obj in self.pool.get('level.sub.faktor.evaluasi.jabatan').browse(cr, uid, level_id, context=context):
              res['value']['level_value'] =level_obj.level_value   
        return res
    def create(self, cr, uid, vals, context=None):
           level_pool = self.pool.get('level.sub.faktor.evaluasi.jabatan')

           if not 'level_value' in vals or not vals['level_value']:
              if vals['level_id'] :
                      vals['level_value'] = level_pool.browse(cr, uid,  vals['level_id'], context=context)[0].level_value
           if not 'level_min_value' in vals or not vals['level_min_value']:
                    if vals['level_min_id'] :
                      vals['level_min_value'] = level_pool.browse(cr, uid,  vals['level_min_id'], context=context)[0].level_value
           return super(evaluasi_lingkungan_pekerjaan_uraian, self).create(cr, uid, vals, context)
           
    _columns = {
        'name'      : fields.many2one('sub.faktor.evaluasi.jabatan',"Sub Faktor",domain=[('jenis_faktor_jabatan','=','lingkungan_pekerjaan')],required=True ),
        'level_min_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Std.Minimal',domain=[('jenis_faktor_jabatan','=','lingkungan_pekerjaan')],required=True),
        'level_id'  : fields.many2one('level.sub.faktor.evaluasi.jabatan', 'Level Anda',domain=[('jenis_faktor_jabatan','=','lingkungan_pekerjaan')],required=True),
        'evaluasi_id': fields.many2one('uraian.evaluasi.jabatan', 'Evaluasi Jabatan',required=False),
        'level_min_value': fields.related('level_min_id','level_value', type='float', string='Nilai Std.Minimal', store=False,readonly=True),
        'level_value': fields.related('level_id','level_value', type='float', string='Nilai Anda', store=False,readonly=True),
    }
evaluasi_lingkungan_pekerjaan_uraian()