# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-07-19 : 1. Init
#  
##############################################################################
import openerp.addons.decimal_precision as dp

from openerp import SUPERUSER_ID
from openerp.osv import fields
from openerp.osv import osv

class uraian_tahapan_kerja_jabatan(osv.Model):
    _name = "uraian.tahapan.kerja.jabatan"
    _description = "Tahapan Kerja Jabatan Per Uraian"
    _columns = {
        'template_id'       : fields.many2one('informasi.jabatan', 'Template',required=False),
        'uraian_id'       : fields.many2one('uraian.tugas.jabatan', 'Uraian',required=False),
        'tahapan_ids'    : fields.one2many('tahapan.kerja','uraian_id',string='Tahapan'),
        'active'  : fields.boolean('Active'),
    }
  
uraian_tahapan_kerja_jabatan()
class tahapan_kerja_jabatan(osv.Model):
    _name = "tahapan.kerja"
    _description = "Tahapan Kerja Jabatan"
    _columns = {
        'uraian_id'       : fields.many2one('uraian.tahapan.kerja.jabatan', 'Uraian',required=False),
        'name'              : fields.char("Tahapan",size=150, required=False ),
        'hasil_ids'    : fields.one2many('tahapan.hasil.kerja','tahapan_id',string='Hasil'),
    }
   
tahapan_kerja_jabatan()
class tahapan_hasil_kerja_jabatan(osv.Model):
    _name = "tahapan.hasil.kerja"
    _description = "Tahapan Hasil Kerja"
    _columns = {
        'name'      : fields.many2one('item.hasil.kerja', 'Hasil Kerja',required=False),
        'jumlah_satuan'     : fields.integer('Jumlah'),
        'satuan_id'         : fields.many2one('satuan.hasil.kerja', 'Satuan',required=False),
        'waktu_kerja'       : fields.integer('Waktu Penyelesaian Dlm 1 Tahun (Menit) ',required=False),
        'tahapan_id': fields.many2one('tahapan.kerja', 'Tahapan',required=False),
    }
tahapan_hasil_kerja_jabatan()