# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-06-23 : 1. Init
#  
##############################################################################
import openerp.addons.decimal_precision as dp

from openerp.osv import fields
from openerp.osv import osv



#======================== Template Jabatan ====================#
class template_jabatan(osv.Model):
    _name = 'template.jabatan'   
    _columns = {
        'job_type'                  : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=True),
        'company_id'    		   	: fields.many2one('res.company', 'OPD', required=True,domain=[('employee_id_kepala_instansi','!=',False)] ),
        'department_id'				: fields.many2one('partner.employee.department', 'Unit Kerja', required=True,), 
        'eselon_id'					: fields.many2one('partner.employee.eselon', 'Eselon'),
        'tingkat_jabatan_id'        : fields.many2one('tingkat.jabatan', 'Tingkat Jabatan'),
        'jenjang_jabatan_id'        : fields.many2one('jenjang.jabatan', 'Jenjang Jabatan'),
        'name'                      : fields.char('Nama Jabatan',size=120, required=True,),    
        'code'                      : fields.char('Kode Jabatan',size=25, required=True,),    
        'parent_id'     			: fields.many2one('template.jabatan', 'Jabatan Atasan',  ),
        'level'  					: fields.integer('Level Jabatan',required=True),

        'uraian_tugas_opd_ids'          : fields.one2many('template.uraian.tugas.opd','template_id',string='Uraian Tugas OPD'),
        'indikator_kinerja_opd_ids'     : fields.one2many('template.indikator.kinerja.opd','template_id',string='Indikator Kinerja OPD',help='Sesuai RPJMD'),
        'indikator_kinerja_jabatan_ids' : fields.one2many('template.indikator.kinerja.jabatan','template_id',string='Indikator Kinerja Jabatan',help='Sesuai Renstra'),
        
        'ikhtisar'					: fields.text('Ikhitsar Jabatan Template',required=True),
        'uraian_tugas_ids'			: fields.one2many('template.uraian.tugas.jabatan','template_id',string='Uraian Tugas'),

 		#syarat
        'golongan_id'		            : fields.many2one('partner.employee.golongan', 'Pangkat/Gol.Ruang Ideal',required=False),
        'jenjang_pendidikan_id'         : fields.many2one('partner.employee.study.type', 'Pendidikan Ideal',required=False,),
        'program_studi_id': fields.many2one('partner.employee.study', 'Program Studi Ideal',),
        #'diklat_perjenjangan'		    : fields.many2many('diklat.perjenjangan.tag',string="Per Jenjangan Ideal"),
        #'diklat_teknis'				    : fields.many2many('diklat.teknis.tag',string="Teknis Ideal",required=False ),

        #'pengalaman_kerja'              : fields.one2many('syarat.pengalaman.kerja.jabatan','template_id',string='Pengalaman Kerja Ideal'),
        'pengetahuan_kerja'				: fields.many2many('pengetahuan.kerja.tag',string="Pengatahuan Kerja Ideal",required=False ),
        'keterampilan_kerja'			: fields.many2many('keterampilan.kerja.tag',string="Keterampilan Kerja Ideal",required=False ),
        #bakat kerja : {intelegensia/verbal/dll}
        'bakat_kerja'		             : fields.many2many('bakat.kerja.tag',string="Bakat Kerja Ideal",required=False ),
        #'bakat_kerja_verbal'			: fields.many2many('bakat.kerja.verbal.tag',string="Verbal Ideal",required=False ),
        #'bakat_kerja_ketelitian'		: fields.many2many('bakat.kerja.ketelitian.tag',string="Ketelitian Ideal",required=False ),
         #templatemen kerja : {d/t/f/,,,}
        'tempramen_kerja'				: fields.many2many('tempramen.kerja.tag',string="Tempramen Kerja Ideal",required=False ),
        #'tempramen_kerja_t'				: fields.many2many('tempramen.kerja.t.tag',string="Tempramen T Ideal",required=False ),

        'minat_kerja'				: fields.many2many('minat.kerja.tag',string="Minat Kerja Ideal",required=False ),
      

         #kondisi Fisik :
        'jenis_kelamin': fields.selection([('None', 'Tidak Melihat Jenis Kelamin'),('L', 'Laki-Laki'), ('P', 'Perempuan') ], 'Jenis Kelamin Ideal'),
        'umur':fields.many2one('umur.tag',string="Umur Ideal",required=False ),
        'tinggi_badan':fields.many2one('tinggi.badan.tag',string="Tinggi Badan Ideal",required=False ),
        'berat_badan':fields.many2one('berat.badan.tag',string="Berat Badan Ideal",required=False ),
        #'postur_badan':fields.many2many('postur.badan.tag',string='Postur Badan Ideal'),
        #'penampilan':fields.many2many('penampilan.tag',string='Penampilan Ideal'),
        'fungsi_pekerjaan':fields.many2many('fungsi.pekerjaan.tag',string='Fungsi Pekerjaan Ideal'),
         ### end syarat
        
    }
    _order = 'name'

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    #_constraints = [(_check_unique_name_insesitive, 'Error Terjadi Duplikasi Data: Nama Jabatan Di OPD Ini Sudah Diinputkan ', ['name','company_id'])]
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        res = {'value': {
                        'uraian_tugas_opd_ids': False,
                        'indikator_kinerja_opd_ids': False,
                        }
                }
        
        if company_id:
            company_pool = self.pool.get('res.company')
            company_obj = company_pool.browse(cr, uid, company_id,context=None)
            if company_obj :
                
                #one2many fields
                uraian_tugas_opd_ids = []
                for _child_obj in company_obj.uraian_tugas_opd_ids:
                    uraian_tugas_opd_ids.append((0,0,{  'name':_child_obj.name,
                                                    'template_id':_child_obj.id,
                                                  
                                                }
                                            ))
                indikator_kinerja_opd_ids = []
                for _child_obj in company_obj.indikator_kinerja_opd_ids:
                    indikator_kinerja_opd_ids.append((0,0,{   'name':_child_obj.name,
                                                    'template_id':_child_obj.id,
                                                   
                                                }
                                            ))

                indikator_kinerja_jabatan_ids = []
                for _child_obj in company_obj.indikator_kinerja_jabatan_opd_ids:
                    indikator_kinerja_jabatan_ids.append((0,0,{   'name':_child_obj.name,'long_name':_child_obj.name,
                                                    'template_id':_child_obj.id,
                                                   
                                                }
                                            ))

                res['value']['uraian_tugas_opd_ids'] = uraian_tugas_opd_ids
                res['value']['indikator_kinerja_opd_ids'] = indikator_kinerja_opd_ids
                res['value']['indikator_kinerja_jabatan_ids'] = indikator_kinerja_jabatan_ids
               

        return res

template_jabatan()


# ------------------------ atribut informasi jabatan
# untuk atribut opd
class template_uraian_tugas_opd(osv.Model):
    _name = "template.uraian.tugas.opd"
    _description = "Template Uraian Tugas OPD"
    _columns = {
        'name'      : fields.char("Uraian Tugas OPD",size=512, required=True ),
        'template_id': fields.many2one('template.jabatan', 'Template',required=False),
    }
template_uraian_tugas_opd()
class template_indikator_kinerja_opd(osv.Model):
    _name = "template.indikator.kinerja.opd"
    _description = "Template Indikator Kinerja OPD"
    _columns = {
        'name'      : fields.char("Indikator Kinerja OPD",size=512, required=True ),
        'template_id': fields.many2one('template.jabatan', 'Template',required=False),
    }
template_indikator_kinerja_opd()
# untuk atribut jabatan
class template_indikator_kinerja_jabatan(osv.Model):
    _name = "template.indikator.kinerja.jabatan"
    _description = "Template Indikator Kinerja Jabatan"
    _columns = {
        'long_name'      : fields.char("Indikator Kinerja Jabatan (Long) ",size=512, required=True ),
        'name'      : fields.char("Indikator Kinerja Jabatan",size=512, required=True ),
        'template_id': fields.many2one('template.jabatan', 'Template',required=False),
    }
template_indikator_kinerja_jabatan()

# belum digunakan
class template_prestasi_kerja_jabatan(osv.Model):
    _name = "template.prestasi.kerja.jabatan"
    _description = "Template Prestasi Kerja Jabatan Jabatan"

    def _get_total_beban_kerja(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _obj in self.browse(cr, uid, ids, context=context):
            res[_obj.id] = 0           
                
        return res
    def _get_total_pegawai(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _obj in self.browse(cr, uid, ids, context=context):
            res[_obj.id] = 0.1           
                
        return res

    _columns = {
        'uraian_kerja_id': fields.many2one('template.uraian.tugas.jabatan', 'Uraian Tugas',required=True),
        'template_id': fields.many2one('template.jabatan', 'Template',required=True),

        'satuan_kuantitas_output'      : fields.many2one('satuan.hitung', 'Satuan Hasil' ,required=True),
        'waktu_penyelesaian'		   : fields.integer('Waktu Penyelesaian (Menit) ',required=True),
        'waktu_kerja_efektif'		   : fields.integer('Waktu Kerja Efektif (Menit) ',required=True),
        'fn_beban_kerja'			   : fields.function(_get_total_beban_kerja, method=True, readonly=True,
        									string='Beban Kerja', store=False),
        'fn_total_pegawai'			   : fields.function(_get_total_pegawai, method=True, readonly=True,
        									string='Pegawai Yang Dibutuhkan', store=False),
    }
template_prestasi_kerja_jabatan()

#Persyaratan 
class diklat_perjenjangan_tag(osv.Model):
    _name = 'diklat.perjenjangan.tag'
    _description = 'Diklat perjenjangan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
diklat_perjenjangan_tag()
class diklat_teknis_tag(osv.Model):
    _name = 'diklat.teknis.tag'
    _description = 'Diklat teknis Tag'
    _columns = {
        'name'             : fields.char('Nama Diklat',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
diklat_teknis_tag()
class pengetahuan_kerja_tag(osv.Model):
    _name = 'pengetahuan.kerja.tag'
    _description = 'pengetahuan_kerja Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
pengetahuan_kerja_tag()
class keterampilan_kerja_tag(osv.Model):
    _name = 'keterampilan.kerja.tag'
    _description = 'keterampilan_kerja Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
keterampilan_kerja_tag()
class bakat_kerja_tag(osv.Model):
    _name = 'bakat.kerja.tag'
    _description = 'Bakat Kerja Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
bakat_kerja_tag()
class bakat_kerja_intelegensia_tag(osv.Model):
    _name = 'bakat.kerja.intelegensia.tag'
    _description = 'bakat_kerja_intelegensia Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
bakat_kerja_intelegensia_tag()
class bakat_kerja_verbal_tag(osv.Model):
    _name = 'bakat.kerja.verbal.tag'
    _description = 'bakat_kerja_verbal Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
bakat_kerja_verbal_tag()
class bakat_kerja_ketelitian_tag(osv.Model):
    _name = 'bakat.kerja.ketelitian.tag'
    _description = 'bakat_kerja_ketelitian Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
bakat_kerja_ketelitian_tag()
class tempramen_kerja_d_tag(osv.Model):
    _name = 'tempramen.kerja.d.tag'
    _description = 'tempramen_kerja_d Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
tempramen_kerja_d_tag()
class tempramen_kerja_t_tag(osv.Model):
    _name = 'tempramen.kerja.t.tag'
    _description = 'tempramen_kerja_t Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
tempramen_kerja_t_tag()
class tempramen_kerja_tag(osv.Model):
    _name = 'tempramen.kerja.tag'
    _description = 'tempramen_kerja Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
tempramen_kerja_tag()
class minat_kerja_tag(osv.Model):
    _name = 'minat.kerja.tag'
    _description = 'minat_kerja Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
minat_kerja_tag()
class upaya_fisik_tag(osv.Model):
    _name = 'upaya.fisik.tag'
    _description = 'upaya_fisik Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
upaya_fisik_tag()
class postur_badan_tag(osv.Model):
    _name = 'postur.badan.tag'
    _description = 'postur_badan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
postur_badan_tag()
class penampilan_tag(osv.Model):
    _name = 'penampilan.tag'
    _description = 'penampilan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
penampilan_tag()
class berat_badan_tag(osv.Model):
    _name = 'berat.badan.tag'
    _description = 'berat badan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
berat_badan_tag()
class tinggi_badan_tag(osv.Model):
    _name = 'tinggi.badan.tag'
    _description = 'tinggi badan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
tinggi_badan_tag()
class umur_tag(osv.Model):
    _name = 'umur.tag'
    _description = 'umur Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
umur_tag()
class fungsi_pekerjaan_tag(osv.Model):
    _name = 'fungsi.pekerjaan.tag'
    _description = 'Fungsi Pekerjaan Tag'
    _columns = {
        'name'             : fields.char('Tag',size=120,required=True),  
        'description'      : fields.text('Deskripsi'),       
    }
fungsi_pekerjaan_tag()
class pengalaman_kerja_tag(osv.Model):
    _name = 'pengalaman.kerja.tag'
    _description = 'Pengalaman Pekerjaan Tag'
    _columns = {
        'name'             : fields.char('Jabatan',size=200,required=True),  
      'company_id'              : fields.many2one('res.company', 'OPD', required=True,domain=[('employee_id_kepala_instansi','!=',False)] ),
    }
pengalaman_kerja_tag()

class pengalaman_kerja_lain_tag(osv.Model):
    _name = 'pengalaman.kerja.lain.tag'
    _description = 'Pengalaman Pekerjaan Lain'
    _columns = {
        'name'             : fields.char('Jabatan',size=200,required=True),  
    }
pengalaman_kerja_lain_tag()
class pengalaman_kerja_opd_lain_tag(osv.Model):
    _name = 'pengalaman.kerja.opd.lain.tag'
    _description = 'Pengalaman OPD Lain Tag'
    _columns = {
        'name'             : fields.char('Instansi/OPD',size=200,required=True),  
    }
pengalaman_kerja_opd_lain_tag()

class syarat_pengalaman_kerja_jabatan(osv.Model):
    _name = "syarat.pengalaman.kerja.jabatan"
    _description = "Syarat Pengalaman Kerja Jabatan"
    _columns = {
      'pengalaman_id': fields.many2one('pengalaman.kerja.tag', 'Jabatan',required=True),
             'company_id'             : fields.many2one('res.company', 'OPD', required=False,domain=[('employee_id_kepala_instansi','!=',False)] ),
       'waktu'         : fields.integer('Masa Kerja (Tahun)',required=True), 
        'template_id': fields.many2one('template.jabatan', 'Template',required=False),
    }
syarat_pengalaman_kerja_jabatan()
# -- testing 
