# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2015
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID

# ====================== res company ================================= #

class res_company(osv.Model):
    _inherit = 'res.company'
    _columns = {
        'uraian_tugas_opd_ids'          : fields.one2many('uraian.tugas.opd','company_id',string='Uraian Tugas OPD'),
        'indikator_kinerja_opd_ids'     : fields.one2many('indikator.kinerja.opd','company_id',string='Indikator Kinerja OPD',help='Sesuai RPJMD'),
        'indikator_kinerja_jabatan_opd_ids'     : fields.one2many('indikator.kinerja.jabatan.opd','company_id',string='Indikator Kinerja Jabatan',help='Sesuai RPJMD'),
    }

res_company()


class uraian_tugas_opd(osv.Model):
    _name = "uraian.tugas.opd"
    _description = "Uraian Tugas OPD"
   
    _columns = {
        'name'      : fields.char("Uraian Tugas", size=512,required=True),
        'company_id': fields.many2one('res.company', 'OPD',required=True),
    }

uraian_tugas_opd()


class indikator_kinerja_opd(osv.Model):
    _name = "indikator.kinerja.opd"
    _description = "Indikator Kinerja OPD"
   
    _columns = {
        'name'      : fields.char("Indikator Kinerja", size=512,required=True),
        'company_id': fields.many2one('res.company', 'OPD',required=True),
    }

indikator_kinerja_opd()

class indikator_kinerja_jabatan_opd(osv.Model):
    _name = "indikator.kinerja.jabatan.opd"
    _description = "Indikator Kinerja Jabatan OPD"
   
    _columns = {
        'name'      : fields.char("Indikator Kinerja Jabatan OPD", size=512,required=True),
        'company_id': fields.many2one('res.company', 'OPD',required=True),
    }

indikator_kinerja_jabatan_opd()

