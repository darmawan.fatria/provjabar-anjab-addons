{
    "name":"Analisa Jabatan",
    "version":"0.1",
    "author":"Darmawan Fatriananda",
    "website":"-",
    "category":"Employee/Jabatan",
    "description": """
        Analisa Jabatan
    """,
    "depends":["df_partner_employee","df_project","df_jabatan_fungsional_tertentu"],
    "init_xml":[],
    "demo_xml":[],
    "data":[    'security/analisa_jabatan_security.xml',
                "company_view.xml",
                "template_jabatan_view.xml",
                "template_uraian_tugas_view.xml",
                "informasi_jabatan_view.xml",
                "analisa_jabatan_config_view.xml",
                'security/ir.model.access.csv',
                  ],
    "active":False,
    "installable":True
}
