# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-06-23 : 1. Init
#  
##############################################################################
import openerp.addons.decimal_precision as dp

from lxml import etree
from openerp import SUPERUSER_ID
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _

#======================== Konversi Satuan Produk ====================#
class informasi_jabatan(osv.Model):
    _name = 'informasi.jabatan'
    _description = 'Informasi Jabatan'
    _inherit='template.jabatan'


    def onchange_template_jabatan(self, cr, uid, ids, template_jabatan_id, context=None):
        res = {'value': {
                        'name': False,
                        'code': False,
                        }
                }
        
        if template_jabatan_id:
            template_jabatan_pool = self.pool.get('template.jabatan')
            tempate_jabatan_obj = template_jabatan_pool.browse(cr, uid, template_jabatan_id)
            if tempate_jabatan_obj :
                res['value']['name'] = tempate_jabatan_obj.name
                res['value']['code'] = tempate_jabatan_obj.code
                res['value']['company_id'] = tempate_jabatan_obj.company_id.id
                res['value']['department_id'] = tempate_jabatan_obj.department_id and tempate_jabatan_obj.department_id.id
                res['value']['job_type'] = tempate_jabatan_obj.job_type
                res['value']['eselon_id'] = tempate_jabatan_obj.eselon_id and tempate_jabatan_obj.eselon_id.id
                res['value']['ikhtisar'] = tempate_jabatan_obj.ikhtisar
                res['value']['level'] = tempate_jabatan_obj.level

                #one2many fields
                uraian_tugas_opd_ids = []
                for _child_obj in tempate_jabatan_obj.uraian_tugas_opd_ids:
                    uraian_tugas_opd_ids.append((0,0,{  'name':_child_obj.name, } ))

                indikator_kinerja_opd_ids = []
                for _child_obj in tempate_jabatan_obj.indikator_kinerja_opd_ids:
                    indikator_kinerja_opd_ids.append((0,0,{  'name':_child_obj.name, } ))

                indikator_kinerja_jabatan_ids = []
                for _child_obj in tempate_jabatan_obj.indikator_kinerja_jabatan_ids:
                    indikator_kinerja_jabatan_ids.append((0,0,{  'name':_child_obj.name, 'long_name':_child_obj.long_name, } ))
                
                
                uraian_tugas_ids = []
                for _child_obj in tempate_jabatan_obj.uraian_tugas_ids:
                    uraian_tugas_ids.append((0,0,{  'name':_child_obj.name, 
                                                    'template_uraian_id':_child_obj.id,
                        } ))

                
                res['value']['uraian_tugas_opd_ids'] = uraian_tugas_opd_ids
                res['value']['indikator_kinerja_opd_ids'] = indikator_kinerja_opd_ids                                              
                res['value']['indikator_kinerja_jabatan_ids'] = indikator_kinerja_jabatan_ids
                res['value']['uraian_tugas_ids'] = uraian_tugas_ids
                
        return res
    def _get_nilai_evaluasi_jabatan(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           nilai_evaluasi_jabatan = 0;
           for child in data.evaluasi_jabatan_ids:
                if child.total_nilai_uraian :
                    nilai_evaluasi_jabatan+= child.total_nilai_uraian;
           res[data.id] = nilai_evaluasi_jabatan
        return res
    def _get_total_nilai_waktu_kerja(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
           nilai = 0;
           for child in data.hasil_kerja_ids:
                if child.total_waktu_kerja :
                    nilai+= child.total_waktu_kerja;
           res[data.id] = nilai
        return res


    
    _columns = {
        'user_job_type'                  : fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan Pegawai',required=False),
        'template_jabatan_id'      : fields.many2one('template.jabatan', 'Jabatan', required=True, ),
        'parent_id'                : fields.many2one('informasi.jabatan', 'Atasan', required=False, ),
        'company_id'               : fields.many2one('res.company', 'OPD', required=True, ),
        'partner_id'               : fields.many2one('res.partner', 'Pegawai', required=True,domain=[('employee','=',True)], ),
        'user_id'                   : fields.many2one('res.users','User Login', readonly=True ), #related on partner

        'state'                     : fields.selection([('draft', 'Draft'), ('bkd', 'Verifikasi'), ('done', 'Selesai'), ('cancel', 'Batal')], 'Status'),
        'stage'                     : fields.selection([('1', 'Init'), ('2', 'Ikhtisar')
                                            , ('3', 'Uraian'), ('4', 'Std Kompetensi')
                                            , ('5', 'Evaluasi Jabatan'), ('6', 'Hasil Kerja'), ('7', 'Tahapan'),('8','Kualifikasi Jabatan'),('9','Selesai'),('99','Send')], 'Tahapan Pengisian'),
        'ikhtisar_jabatan'                  : fields.text('Ikhitsar Jabatan',help="Ikhtisar jabatan baru"),
        'use_ikhtisar_template'         : fields.selection([('ya', 'Terima'), ('tidak', 'Tidak Terima')], 'Menggunakan Ikhtisar Dari Template ?'),

        #override oneToMany
        'uraian_tugas_opd_ids'          : fields.one2many('informasi.uraian.tugas.opd','template_id',string='Uraian Tugas OPD'),
        'indikator_kinerja_opd_ids'     : fields.one2many('informasi.indikator.kinerja.opd','template_id',string='Indikator Kinerja OPD',help='Sesuai RPJMD'),
        'indikator_kinerja_jabatan_ids' : fields.one2many('indikator.kinerja.jabatan','template_id',string='Indikator Kinerja Jabatan',help='Sesuai Renstra'),
        'uraian_tugas_ids'              : fields.one2many('uraian.tugas.jabatan','template_id',string='Uraian Tugas'),

        #dari uraian
        #'tahapan_kerja_ids'              : fields.one2many('tahapan.kerja.jabatan','template_id',string='Tahapan Kerja'),
        'standar_kompetensi_ids'         : fields.one2many('standar.kompetensi.jabatan','template_id',string='Standar Kompetensi'),
        'evaluasi_jabatan_ids'           : fields.one2many('uraian.evaluasi.jabatan','template_id',string='Evaluasi Jabatan'),
        'hasil_kerja_ids'               : fields.one2many('uraian.hasil.kerja.jabatan','template_id',string='Evaluasi Jabatan'),
        'tahapan_kerja_ids'               : fields.one2many('uraian.tahapan.kerja.jabatan','template_id',string='Tahapan Kerja'),
        
       'nilai_evaluasi_jabatan' :fields.function(_get_nilai_evaluasi_jabatan, method=True, string='Nilai Evaluasi Jabatan', type='float', readonly=True,store=False),
       'total_nilai_waktu_kerja' :fields.function(_get_total_nilai_waktu_kerja, method=True, string='Total Nilai Anggaran Kerja', type='float', readonly=True,store=False),

        #syarat
        'exist_golongan_id'                   : fields.many2one('partner.employee.golongan', 'Pangkat/Gol.Ruang',required=False),
        'exist_jenjang_pendidikan_id'         : fields.many2one('partner.employee.study.type', 'Pendidikan',required=False,),
        'exist_program_studi_id': fields.many2one('partner.employee.study', 'Program Studi',),

        'diklat_perjenjangan'               : fields.many2many('diklat.perjenjangan.tag','diklat_perjenjangan_ideal_rel','informasi_jabatan_id','diklat_perjenjangan_tag_id',"Per Jenjangan Ideal"),
        'exist_diklat_perjenjangan'       : fields.many2many('diklat.perjenjangan.tag','diklat_perjenjangan_exist_rel','informasi_jabatan_id','diklat_perjenjangan_tag_id',"Perjenjangan"),
        'exist_diklat_teknis'                 : fields.many2many('diklat.teknis.tag','diklat_teknis_exist_rel','informasi_jabatan_id','diklat_teknis_tag_id',"Teknis",),
        'diklat_teknis'                         : fields.many2many('diklat.teknis.tag','diklat_teknis_ideal_rel','informasi_jabatan_id','diklat_teknis_tag_id',"Teknis Ideal",),

        'pengalaman_kerja'                        : fields.one2many('syarat.pengalaman.kerja.pegawai','template_id',string='Pengalaman Kerja Syarat Minimal', ),
        'exist_pengalaman_kerja'              : fields.one2many('pengalaman.kerja.pegawai','template_id',string='Pengalaman Kerja Anda',),
        'exist_pengetahuan_kerja'             : fields.many2many('pengetahuan.kerja.tag',string="Pengatahuan Kerja",required=False ),
        'exist_keterampilan_kerja'            : fields.many2many('keterampilan.kerja.tag',string="Keterampilan Kerja",required=False ),
        #bakat kerja : 
        'exist_bakat_kerja'                    : fields.many2many('bakat.kerja.tag',string="Bakat Kerja",required=False ),
         #templatemen kerja : 
         'exist_tempramen_kerja'              : fields.many2many('tempramen.kerja.tag',string="Tempramen Kerja",required=False ),

         'exist_minat_kerja'               : fields.many2many('minat.kerja.tag',string="Minat Kerja",required=False ),
        

         #kondisi Fisik :
        'exist_jenis_kelamin': fields.selection([('L', 'Laki-Laki'), ('P', 'Perempuan'), ], 'Jenis Kelamin'),
        'exist_umur':fields.integer('Umur'),
        'exist_tinggi_badan':fields.integer('Tnggi Badan'),
        'exist_berat_badan':fields.integer('Berat Badan'),

        'postur_badan':fields.many2many('postur.badan.tag','postur_badan_ideal_rel','informasi_jabatan_id','postur_badan_tag_id','Postur Badan Ideal'),
        'exist_postur_badan':fields.many2many('postur.badan.tag','postur_badan_exist_rel','informasi_jabatan_id','postur_badan_tag_id','Postur Badan'),

        'penampilan':fields.many2many('penampilan.tag','penampilan_ideal_rel','informasi_jabatan_id','penampilan_tag_id','Penampilan Ideal'),
        'exist_penampilan':fields.many2many('penampilan.tag','penampilan_exist_rel','informasi_jabatan_id','penampilan_tag_id','Penampilan'),
        
        'upaya_fisik'             : fields.many2many('upaya.fisik.tag','upaya_fisik_ideal_rel','informasi_jabatan_id','upaya_fisik_tag_id',"Upaya Fisik Ideal",required=False ),
        'exist_upaya_fisik'               : fields.many2many('upaya.fisik.tag','upaya_fisik_exist_rel','informasi_jabatan_id','upaya_fisik_tag_id',"Upaya Fisik",required=False ),

       
        
        
        

        'exist_fungsi_pekerjaan':fields.many2many('fungsi.pekerjaan.tag',string='Fungsi Pekerjaan'),
         ### end syarat
         'user_id_bkd': fields.many2one('res.users', 'Pejabat Pengevaluasi (BKD)',),
         'active'        : fields.boolean('Active'),
       
    }
    _defaults = {
        'user_id': lambda self, cr, uid, context: uid,
        'company_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'partner_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).partner_id.id,
        'user_job_type': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).partner_id.job_type,
        'state' :'draft',
        'use_ikhtisar_template':'ya',
        'stage':'1',
        'active':True,
         'user_id_bkd': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.user_id_bkd.id,
    }

  
    def create(self, cr, uid, vals, context=None):
            
            if uid:
                exist_info_obj = self.search(cr, uid, [('user_id', '=', uid),('active', '=', True)], context=None)
                if exist_info_obj:
                    raise osv.except_osv(_('Invalid Action!'),
                                                 _('Tidak Boleh Mengisi Informasi Jabatan Lebih Dari 1 Kali, SIlahkan Batalkan Terlebih Dahulu Data Sebelumnya.'))

            template_jabatan_id = vals['template_jabatan_id'];
            template_pool = self.pool.get('template.jabatan');
            for obj in template_pool.browse(cr, uid, template_jabatan_id, context=context):
                if not 'ikhtisar' in vals or not vals['ikhtisar']:
                    vals['ikhtisar'] = obj.ikhtisar
                if not 'department_id' in vals or not vals['department_id']:
                    vals['department_id'] = obj.department_id.id
                if not 'job_type' in vals or not vals['job_type']:
                    vals['job_type'] = obj.job_type
                if not 'eselon_id' in vals or not vals['eselon_id']:
                    vals['eselon_id'] = obj.eselon_id.id
                if not 'tingkat_jabatan_id' in vals or not vals['tingkat_jabatan_id']:
                    vals['tingkat_jabatan_id'] = obj.tingkat_jabatan_id and obj.tingkat_jabatan_id.id or None
                if not 'jenjang_jabatan_id' in vals or not vals['jenjang_jabatan_id']:
                    vals['jenjang_jabatan_id'] = obj.jenjang_jabatan_id and obj.jenjang_jabatan_id.id or None
                
            return super(informasi_jabatan, self).create(cr, uid, vals, context)

    
    def next_stage(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            if not obj.stage :
                self.set_stage(cr,uid,[obj.id],'1',context=None)
                continue;
            if obj.stage == '1' : #ikhtisar jabatan
                self.set_stage(cr,uid,[obj.id],'2',context=None)
                self.set_uraian_tugas_jabatan(cr,uid,[obj.id],context=None)
                continue;
            if obj.stage == '2' :
                if(not self.validation_indikator_kinerja_jabatan(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Silakan Pilih (Minimal 1) Indikator Kinerja Jabatan'))
                self.set_stage(cr,uid,[obj.id],'3',context=None)
                continue;
            if obj.stage == '3' : #standar kompetensi
                if(not self.validation_uraian_tugas_jabatan(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Silakan Pilih (Minimal 1) Uraian Tugas Dan Sesuaikan Dengan Indikator Kinerja'))
                self.set_stage(cr,uid,[obj.id],'4',context=None)
                self.set_standar_kompetensi_jabatan(cr,uid,[obj.id],context=None)
                self.set_evaluasi_jabatan(cr,uid,[obj.id],context=None)
                self.set_hasil_kerja(cr,uid,[obj.id],context=None)
                self.set_tahapan_kerja(cr,uid,[obj.id],context=None)
                continue;
            if obj.stage == '4' :
                if (not self.validation_standar_kompetensi_manjerial(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Setiap Uraian Tugas Harus Memiliki Minimal 1 Unit Manajerial'))
                self.set_stage(cr,uid,[obj.id],'5',context=None)
                continue;
            if obj.stage == '5' :
                if (not self.validation_evaluasi_jabatan(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Silakan  Lengkapi Faktor Evaluasi Jabatan Pada Tiap Uraian'))
                
                self.set_stage(cr,uid,[obj.id],'6',context=None)
                continue;
            if obj.stage == '6' :
                if (not self.validation_hasil_kerja(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Setiap Uraian Tugas Harus Memiliki Minimal 1 Hasil Kerja'))
                if (not self.validation_menit_hasil_kerja(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Jumlah Total Waktu Pekerjaan Melebihi Maksimal Menit Dalam 1 Tahun (525600 Menit)'))
                    
                self.set_stage(cr,uid,[obj.id],'7',context=None)
                continue;
            if obj.stage == '7' :
                if (not self.validation_tahap_kerja(cr,uid,obj)) :
                    raise osv.except_osv(_('Anda Tidak Dapat Melanjutkan Pengisian!'),
                                             _('Setiap Uraian Harus Memiliki Minimal 3 Tahapan'))

                self.set_stage(cr,uid,[obj.id],'8',context=None)
                continue;
            if obj.stage == '8' :
                #validasi 
                self.set_stage(cr,uid,[obj.id],'9',context=None)
                continue;
        return True;
    def set_stage(self, cr, uid, ids,new_stage ,context=None):
        return self.write(cr, uid, ids, {'stage':new_stage}, context=context)

    def action_draft_done(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'stage':'9'}, context=context)  

    def set_bkd(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'state':'bkd','stage':'99'}, context=context) 
    def set_done(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'state':'done'}, context=context) 
    def set_draft(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_cancel(self, cr, uid, ids,context=None):
        return self.write(cr, uid, ids, {'state':'cancel','active':False}, context=context) 

    def action_propose(self, cr, uid, ids,context=None):
        self.set_bkd(cr, uid, ids, context=context)
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.informasi.jabatan.done',
                'target': 'new',
                'context': context,
            } 
    def action_done(self, cr, uid, ids,context=None):
        return self.set_done(cr, uid, ids, context=context) 
    def action_draft(self, cr, uid, ids,context=None):
        return self.set_draft(cr, uid, ids, context=context)
    def action_tolak(self, cr, uid, ids,context=None):
        self.write(cr, uid, ids, {'stage':'8'}, context=context) 
        return self.set_draft(cr, uid, ids, context=context)
    def action_cancel(self, cr, uid, ids,context=None):
        self.set_cancel(cr, uid, ids, context=context)  
        domain = [('user_id','=',uid)]
        return {
             'type': 'ir.actions.act_window',
             'name': _('Informasi Jabatan'),
             'res_model': 'informasi.jabatan',
             'view_type': 'form',
             'view_mode': 'tree,form',
             'target': 'current',
             'domain': domain,
        }
        
    def rebuild_uraian_tugas(self,cr,uid,ids,context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            self.set_standar_kompetensi_jabatan(cr,uid,[obj.id],context=None)
            self.set_evaluasi_jabatan(cr,uid,[obj.id],context=None)
            self.set_hasil_kerja(cr,uid,[obj.id],context=None)
            self.set_tahapan_kerja(cr,uid,[obj.id],context=None)
        return True;
    def set_uraian_tugas_jabatan(self, cr, uid, ids,context=None):
        in_use_uraian_kerja_ids = [];
        for obj in self.browse(cr, uid, ids, context=context):
            for uraian_obj in obj.uraian_tugas_ids:
                in_use_uraian_kerja_ids.append(uraian_obj.id)

        if in_use_uraian_kerja_ids:
            self.set_stage_for_uraian_tugas(cr,uid,in_use_uraian_kerja_ids,context=None)
        return True
    def set_stage_for_uraian_tugas(self, cr, uid, uraian_ids,context=None):
        uraian_pool = self.pool.get('uraian.tugas.jabatan');
        return uraian_pool.write(cr, uid, uraian_ids, {'is_input_indikator_kinerja':True}, context=context)
    def set_tahapan_kerja_detail(self, cr, uid, ids,context=None):
        tahapan_pool = self.pool.get('tahapan.kerja.jabatan');
        
        for obj in self.browse(cr, uid, ids, context=context):
            
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    tahapan_data =  {}
                    tahapan_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                            })

                    if uraian_obj.template_uraian_id:
                        if uraian_obj.template_uraian_id.tahapan_kerja_ids :
                            for tahapan_obj in uraian_obj.template_uraian_id.tahapan_kerja_ids:
                                tahapan_data.update({ 'name':tahapan_obj.name,
                                    })
                                tahapan_id = tahapan_pool.create(cr, uid, tahapan_data,context)
                    else :
                        tahapan_data.update({ 'name':'-',
                                    })
                        for i in range(0,3):
                            tahapan_id = tahapan_pool.create(cr, uid, tahapan_data,context)

        
        return True;
    
    def set_standar_kompetensi_jabatan(self, cr, uid, ids,context=None):
        standar_kompetensi_pool = self.pool.get('standar.kompetensi.jabatan');
        arr_data = []
        arr_data_not_use =[]
        for obj in self.browse(cr, uid, ids, context=context):
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    standar_kompetensi_data =  {}
                    eselon_obj = obj.eselon_id
                    standar_kompetensi_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'min_level_kompetensi': eselon_obj and eselon_obj.min_level_kompetensi or 0,
                                            'max_level_kompetensi': eselon_obj and eselon_obj.max_level_kompetensi or 0,
                                            'active':True
                            })
                    arr_data.append(standar_kompetensi_data)
                else :
                    arr_data_not_use.append({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            })
        
        #append data
        for data in arr_data:
            exist_ids = standar_kompetensi_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if not exist_ids : 
                new_id = standar_kompetensi_pool.create(cr, uid, data,context)
            
        #delete  exist
        arr_data_to_cancel = []
        for data in arr_data_not_use:
            exist_ids = standar_kompetensi_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if exist_ids :
                arr_data_to_cancel.append(exist_ids[0])

        if arr_data_to_cancel : 
            standar_kompetensi_pool.write(cr, uid, arr_data_to_cancel, {'active':False}, context=context) 

        return True;
    def set_evaluasi_jabatan(self, cr, uid, ids,context=None):
        uraian_evjab_pool = self.pool.get('uraian.evaluasi.jabatan');
        arr_data = []
        arr_data_not_use =[]
        
        for obj in self.browse(cr, uid, ids, context=context):
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    uraian_evjab_data =  {}
                    uraian_evjab_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'job_type':obj.job_type,
                                            'active':True
                            })
                    arr_data.append(uraian_evjab_data)
                else :
                    arr_data_not_use.append({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'job_type':obj.job_type,
                                            })
        
        #append data
        for data in arr_data:
            exist_ids = uraian_evjab_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if not exist_ids : 
                new_id = uraian_evjab_pool.create(cr, uid, data,context)
            
        #delete  exist
        arr_data_to_cancel = []
        for data in arr_data_not_use:
            exist_ids = uraian_evjab_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if exist_ids :
                arr_data_to_cancel.append(exist_ids[0])

        if arr_data_to_cancel : 
            uraian_evjab_pool.write(cr, uid, arr_data_to_cancel, {'active':False}, context=context) 

        
        return True;
    def set_hasil_kerja(self, cr, uid, ids,context=None):
        uraian_hasil_kerja_pool = self.pool.get('uraian.hasil.kerja.jabatan');
        arr_data = []
        arr_data_not_use =[]
        
        for obj in self.browse(cr, uid, ids, context=context):
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    uraian_hasil_kerja_data =  {}
                    uraian_hasil_kerja_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'job_type':obj.job_type,
                                            'active':True
                            })
                    arr_data.append(uraian_hasil_kerja_data)
                else :
                    arr_data_not_use.append({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'job_type':obj.job_type,
                                            })
        
        #append data
        for data in arr_data:
            exist_ids = uraian_hasil_kerja_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if not exist_ids : 
                new_id = uraian_hasil_kerja_pool.create(cr, uid, data,context)
            
        #delete  exist
        arr_data_to_cancel = []
        for data in arr_data_not_use:
            exist_ids = uraian_hasil_kerja_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if exist_ids :
                arr_data_to_cancel.append(exist_ids[0])

        if arr_data_to_cancel : 
            uraian_hasil_kerja_pool.write(cr, uid, arr_data_to_cancel, {'active':False}, context=context) 
        
        return True;
    def set_tahapan_kerja(self, cr, uid, ids,context=None):
        uraian_tahapan_kerja_pool = self.pool.get('uraian.tahapan.kerja.jabatan');
        arr_data = []
        arr_data_not_use =[]
        
        for obj in self.browse(cr, uid, ids, context=context):
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    uraian_tahapan_kerja_data =  {}
                    uraian_tahapan_kerja_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            'active':True
                            })
                    arr_data.append(uraian_tahapan_kerja_data)
                else :
                    arr_data_not_use.append({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                                            })
        
        #append data
        for data in arr_data:
            exist_ids = uraian_tahapan_kerja_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if not exist_ids : 
                new_id = uraian_tahapan_kerja_pool.create(cr, uid, data,context)
            
        #delete  exist
        
        arr_data_to_cancel = []
        for data in arr_data_not_use:
            exist_ids = uraian_tahapan_kerja_pool.search(cr,uid, [
                                               ('template_id', '=', data['template_id']), 
                                               ('uraian_id', '=', data['uraian_id']),                                         
                                               ('active','=',True)
                                               ], 
                                    context=None)
            if exist_ids :
                arr_data_to_cancel.append(exist_ids[0])

        
        if arr_data_to_cancel : 
            uraian_tahapan_kerja_pool.write(cr, uid, arr_data_to_cancel, {'active':False}, context=context) 
        
        return True;


    def old_set_standar_kompetensi_jabatan(self, cr, uid, ids,context=None):
        standar_kompetensi_pool = self.pool.get('standar.kompetensi.jabatan');
        for obj in self.browse(cr, uid, ids, context=context):
            
            for uraian_obj in obj.uraian_tugas_ids:
                if uraian_obj.is_use:
                    standar_kompetensi_data =  {}
                    standar_kompetensi_data.update({ 'template_id':obj.id,
                                            'uraian_id':uraian_obj.id,
                            })
                    if uraian_obj.template_uraian_id:
                        if uraian_obj.template_uraian_id.standar_kompetensi_ids :
                            for standar_kompetensi_obj in uraian_obj.template_uraian_id.standar_kompetensi_ids:
                                if standar_kompetensi_obj.jenis_standar_kompetensi == 'manajerial' :
                                    standar_kompetensi_data.update({ 'level_kompetensi_ideal_manajerial':standar_kompetensi_obj.id,
                                        })
                                elif standar_kompetensi_obj.jenis_standar_kompetensi == 'teknis' :
                                    standar_kompetensi_data.update({ 'level_kompetensi_ideal_teknis':standar_kompetensi_obj.id,
                                        })
                                
                                elif standar_kompetensi_obj.jenis_standar_kompetensi == 'sosiokultural' :
                                    standar_kompetensi_data.update({ 'level_kompetensi_ideal_sosiokultural':standar_kompetensi_obj.id,
                                        })

                                elif standar_kompetensi_obj.jenis_standar_kompetensi == 'pemerintahan' :
                                    standar_kompetensi_data.update({ 'level_kompetensi_ideal_pemerintahan':standar_kompetensi_obj.id,
                                        }) 
                                stad_kompetensi_id = standar_kompetensi_pool.create(cr, uid, standar_kompetensi_data,context)
                        else :
                            stad_kompetensi_id = standar_kompetensi_pool.create(cr, uid, standar_kompetensi_data,context)
                    else :
                        stad_kompetensi_id = standar_kompetensi_pool.create(cr, uid, standar_kompetensi_data,context)
                   
        return True;
#validation =========
    def validation_indikator_kinerja_jabatan(self, cr, uid, obj):
        for o_data in obj.indikator_kinerja_jabatan_ids:
            if o_data.is_use:
                return True;
        return False;
    def validation_uraian_tugas_jabatan(self, cr, uid, obj):
        for o_data in obj.uraian_tugas_ids:
            
            if o_data.is_use:
                return True;
        return False;
    def validation_standar_kompetensi_manjerial(self, cr, uid, obj):
        for o_data in obj.standar_kompetensi_ids:
             if o_data.unit_manajerial_ids : #exit
                if len(o_data.unit_manajerial_ids) == 0: #but zero
                        return False
             else : 
                    return False;  
        return True;
    def validation_evaluasi_jabatan(self, cr, uid, obj):
        total_nilai_uraian=0;
        for ev_jab_obj in obj.evaluasi_jabatan_ids:
            total_nilai_uraian+=ev_jab_obj.total_nilai_uraian
            if ev_jab_obj.total_nilai_uraian<=0:
                    return False;

        if total_nilai_uraian > 0 :
            return True;
        return False;
    def validation_hasil_kerja(self, cr, uid, obj):
        for o_data in obj.hasil_kerja_ids:
             if o_data.hasil_kerja_uraian_ids : #exit
                if len(o_data.hasil_kerja_uraian_ids) == 0: #but zero
                        return False
             else : 
                    return False;  
        return True;
    def validation_menit_hasil_kerja(self, cr, uid, obj):
        total_menit_kerja = 0;
        maks_total_menit_kerja = 525600; # 1 * 60 (1jam) * 24 (sehari 24 jam ) * 365 (1 tahun 365 hari)
        for o_data in obj.hasil_kerja_ids:
             total_menit_kerja += o_data.total_waktu_kerja;

        if (total_menit_kerja > maks_total_menit_kerja) :
            return False;
        return True;
    def validation_tahap_kerja(self, cr, uid, obj):
        for o_data in obj.tahapan_kerja_ids:
             if o_data.tahapan_ids : #exit
                if len(o_data.tahapan_ids) == 0: #but zero
                        return False
                if len(o_data.tahapan_ids) < 3: #but zero
                        return False
             else : 
                    return False;  
        return True;
    
informasi_jabatan()

# ------------------------ atribut informasi jabatan
# untuk atribut jabatan
class informasi_uraian_tugas_opd(osv.Model):
    _name = "informasi.uraian.tugas.opd"
    _description = "Informasi Uraian Tugas OPD"
    _columns = {
        'name'      : fields.char("Uraian Tugas OPD",size=512, required=False ),
        'template_id': fields.many2one('informasi.jabatan', 'Template',required=False),
    }
informasi_uraian_tugas_opd()
class informasi_indikator_kinerja_opd(osv.Model):
    _name = "informasi.indikator.kinerja.opd"
    _description = "Informasi Indikator Kinerja OPD"
    _columns = {
        'name'      : fields.char("Indikator Kinerja OPD",size=512, required=False ),
        'template_id': fields.many2one('informasi.jabatan', 'Template',required=False),
    }
informasi_indikator_kinerja_opd()
class indikator_kinerja_jabatan(osv.Model):
    _name = "indikator.kinerja.jabatan"
    _description = "Indikator Kinerja Jabatan"
    _columns = {
        'name'          : fields.char("Indikator Kinerja Jabatan",size=512, required=False ),
        'is_use'        : fields.boolean('Pilih'),
        'template_id': fields.many2one('informasi.jabatan', 'Template',required=False),
    }
    _defaults = {
        'is_use': False,
    }
indikator_kinerja_jabatan()

class syarat_pengalaman_kerja_pegawai(osv.Model):
    _name = "syarat.pengalaman.kerja.pegawai"
    _description = "Syarat Pengalaman Kerja Pegawai"
    _columns = {
      'pengalaman_id': fields.many2one('pengalaman.kerja.tag', 'Jabatan',required=True),
       'company_id'             : fields.many2one('res.company', 'OPD', required=False,domain=[('employee_id_kepala_instansi','!=',False)] ),
       'waktu'         : fields.integer('Masa Kerja (Tahun)',required=True), 
        'template_id': fields.many2one('informasi.jabatan', 'Template',required=False),
          'jenis_pengalaman'                  : fields.selection([('ideal', 'Ideal'),
                                                      ('exist', 'Exist'),
                                                      ], 
                                             'Jenis Pengalaman',required=False),
    }
syarat_pengalaman_kerja_pegawai()
class pengalaman_kerja_pegawai(osv.Model):
    _name = "pengalaman.kerja.pegawai"
    _description = "Pengalaman Kerja Pegawai"
    _columns = {
      'pengalaman_id': fields.many2one('pengalaman.kerja.lain.tag', 'Jabatan',required=True),
       'company_id': fields.many2one('pengalaman.kerja.opd.lain.tag', 'Instansi',required=True),
       'waktu'         : fields.integer('Masa Kerja (Tahun)',required=True), 
        'template_id': fields.many2one('informasi.jabatan', 'Template',required=False),
    }
pengalaman_kerja_pegawai()

class notification_informasi_jabatan_done(osv.osv_memory):
    _name = "notification.informasi.jabatan.done"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
# sample info jabatan

"""



trik :
Go to:

View -> Indentation

It should read:

Indent using spaces [x]
Tab width: 2
Select:

Convert Indentation to Tabs
Then Select:

Tab width: 4
Convert Indentation to Spaces
Done.
"""
# for delete class
class uraian_korelasi_eksternal(osv.Model):
    _name = "uraian.korelasi.eksternal"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
uraian_korelasi_eksternal()
class uraian_korelasi_internal(osv.Model):
    _name = "uraian.korelasi.internal"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
uraian_korelasi_internal()
class tahapan_korelasi_internal(osv.Model):
    _name = "tahapan.korelasi.internal"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_korelasi_internal()
class tahapan_korelasi_eksternal(osv.Model):
    _name = "tahapan.korelasi.eksternal"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_korelasi_eksternal()
class tahapan_resiko_jabatan(osv.Model):
    _name = "tahapan.resiko.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_resiko_jabatan()
class tahapan_kondisi_jabatan(osv.Model):
    _name = "tahapan.kondisi.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_kondisi_jabatan()
class tahapan_wewenang_jabatan(osv.Model):
    _name = "tahapan.wewenang.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_wewenang_jabatan()

class tahapan_tgjawab_jabatan(osv.Model):
    _name = "tahapan.tgjawab.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_tgjawab_jabatan()
class tahapan_perangkat_kerja_jabatan(osv.Model):
    _name = "tahapan.perangkat.kerja.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_perangkat_kerja_jabatan()
class tahapan_bahan_kerja_jabatan(osv.Model):
    _name = "tahapan.bahan.kerja.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_bahan_kerja_jabatan()
class tahapan_hasil_kerja_jabatan(osv.Model):
    _name = "tahapan.hasil.kerja.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_hasil_kerja_jabatan()
class evaluasi_dampak_uraian(osv.Model):
    _name = "evaluasi.dampak.uraian"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
evaluasi_dampak_uraian()
class tahapan_kerja_jabatan(osv.Model):
    _name = "tahapan.kerja.jabatan"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
tahapan_kerja_jabatan()
class eselon(osv.Model):
    _name = "eselon"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
eselon()
class perangkat(osv.Model):
    _name = "perangkat"
    _columns = {
      'name': fields.char('Notif', size=10),

    }
perangkat()
