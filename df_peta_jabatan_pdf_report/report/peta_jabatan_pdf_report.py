from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale
from string import upper


class report_peta_jabatan_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_peta_jabatan_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_peta_jabatan' : self._get_peta_jabatan,
        })
    def _get_peta_jabatan(self,data):
        obj_data= [] #self.pool.get('peta.jabatan').browse(self.cr,self.uid,data['data_ids'])
        return obj_data
  
class wrapped_report_peta_jabatan(osv.AbstractModel):
    _name = 'report.df_peta_jabatan_pdf_report.report_peta_jabatan_report'
    _inherit = 'report.abstract_report'
    _template = 'df_peta_jabatan_pdf_report.report_peta_jabatan_report'
    _wrapped_report_class = report_peta_jabatan_report