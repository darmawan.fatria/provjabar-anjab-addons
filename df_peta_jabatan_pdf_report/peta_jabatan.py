# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-03-014 : 1. Init  
#  
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time



class peta_jabatan(osv.Model):
    _name = 'peta.jabatan'
    #Laporan
    def download_peta_jabatan(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        datas = {'data_ids':ids,
                 }
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_peta_jabatan_pdf_report.report_peta_jabatan_report', 
                        data=datas, context=context)
        
   
peta_jabatan()