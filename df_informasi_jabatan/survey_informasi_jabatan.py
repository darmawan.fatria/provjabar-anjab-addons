# -*- encoding: utf-8 -*-
##############################################################################
#
#    @author : Darmawan Fatriananda
#    -
#    Copyright (c) 2015 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-06-08 : 1. Init
#  
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp



#======================== Konversi Satuan Produk ====================#
class survey_informasi_jabatan(osv.Model):
    _inherit = 'survey.survey'
   
    
    _columns = {
        'company_id'    		   : fields.many2one('res.company', 'OPD', required=True, ),
    }
    
survey_informasi_jabatan()