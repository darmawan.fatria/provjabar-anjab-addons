{
    "name":"Kuisoner Untuk Evaluasi Jabatan",
    "version":"0.1",
    "author":"Darmawan Fatriananda",
    "website":"-",
    "category":"Employee/Survey",
    "description": """
        Kuisoner Untuk Evaluasi Jabatan
    """,
    "depends":["survey"],
    "init_xml":[],
    "demo_xml":[],
    "data":[
                 "survey_informasi_jabatan_view.xml",
                  ],
    "active":False,
    "installable":True
}
