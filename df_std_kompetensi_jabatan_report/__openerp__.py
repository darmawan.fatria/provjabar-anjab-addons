##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
{
    "name": "Laporan Standar Kompetensi Jabatan",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Reporting /Informasi Jabatan /Standar Kompetensi Jabatan",
    "description": "Laporan Standar Kompetensi Jabatan .",
    "website" : "www.mediasee.net",
    "license" : "",
    "depends": ['df_analisa_jabatan'],
    'data': ["analisa_jabatan_report.xml",],
    'installable': True,
    'active': False,
}
