import openerp.pooler
import time
from datetime import datetime
import openerp.tools
import logging
from openerp.report import report_sxw
from datetime import datetime
from openerp.addons.report_webkit import webkit_report
from openerp.tools.translate import _
from openerp.osv import osv
import math
import locale

class analisa_jabatan_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(analisa_jabatan_report_parser, self).__init__(cr, uid, name, context=context)
    
    
    def get_analisa_jabatan_report_raw(self,filters,context=None):
        obj_pool = self.pool.get('informasi.jabatan')
        company_id=filters['form']['company_id'][0]
        filtered_ids= obj_pool.search(self.cr,self.uid,[('company_id','=',company_id),
                                                        ],order="level asc")
        results = obj_pool.browse(self.cr, self.uid, filtered_ids)
        return results;
    def get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def get_title(self,prefiks,filters):
        #p_tahun_pengajuan=filters['form']['tahun_pengajuan']
      
        return prefiks+' '
   
    
